﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class MyBuilder {

	[UnityEditor.MenuItem("Tools/Build Project AllScene Android")]
	public static void BuildProjectAllSceneAndroid() {
		EditorUserBuildSettings.SwitchActiveBuildTarget (BuildTarget.Android);
		string[] allScene = new string[EditorBuildSettings.scenes.Length];
		int i = 0;
		foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes) {
			allScene[i] = scene.path;
			i++;
		}
		PlayerSettings.bundleIdentifier = "jp.kamasu.unity-sample";
		PlayerSettings.statusBarHidden = true;
		string errorMsg_Device = BuildPipeline.BuildPlayer (allScene,
		                         	 "unity-sample.apk",
		                          	 BuildTarget.Android,
		                        	 BuildOptions.None);
		if (string.IsNullOrEmpty (errorMsg_Device)) {
			Debug.Log ("[Android Build] Succeeded.");
		} else {
			Debug.LogError("[Android Build] Failed.");
			Debug.LogError(errorMsg_Device);
		}
	}

	[UnityEditor.MenuItem("Tools/Build Project AllScene iOS")]
	public static void BuildProjectAllSceneiOS() {
		EditorUserBuildSettings.SwitchActiveBuildTarget (BuildTarget.iPhone);
		string[] allScene = new string[EditorBuildSettings.scenes.Length];

		int i = 0;
		foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes) {
			allScene[i] = scene.path;
			i++;
		}

		BuildOptions opt = BuildOptions.SymlinkLibraries | BuildOptions.AllowDebugging | BuildOptions.ConnectWithProfiler |
						BuildOptions.Development;

		PlayerSettings.iOS.sdkVersion = iOSSdkVersion.DeviceSDK;
		PlayerSettings.bundleIdentifier = "kamasujp.com";
		PlayerSettings.statusBarHidden = true;

		string errorMsg_Device = BuildPipeline.BuildPlayer (
			allScene,
			"ios",
			BuildTarget.iPhone,
			opt);
		if (string.IsNullOrEmpty (errorMsg_Device)) {
			Debug.Log ("[iOS Build] Succeeded.");
		} else {
			Debug.LogError ("[iOS Build] Failed.");
			Debug.LogError(errorMsg_Device);
		}

		PlayerSettings.iOS.sdkVersion = iOSSdkVersion.SimulatorSDK;
	}
}
